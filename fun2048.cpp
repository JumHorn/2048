#include <QKeyEvent>
#include "fun2048.h"
#include "ui_fun2048.h"
#include "board.h"

Fun2048::Fun2048(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Fun2048)
{
	ui->setupUi(this);
	board = new Board();
	QGridLayout *layout = new QGridLayout(ui->widget);
	layout->setSpacing(0);
	layout->setVerticalSpacing(0);
	layout->installEventFilter(this);
	for (int i = 0; i < 4 * 4; i++)
	{
		auto button = new QPushButton(ui->widget);
		button->setFixedWidth(100);
		button->setFixedHeight(100);
		button->installEventFilter(this);
		gridbuttons.append(button);
		layout->addWidget(button, i / 4, i % 4);
	}
	ui->widget->hide();
	createPalette();
	setFocus();
}

Fun2048::~Fun2048()
{
	delete ui;
}

void Fun2048::createPalette()
{
	palette[0] = "white";
	palette[2] = "green";
	palette[4] = "pink";
	palette[8] = "magenta";
	palette[16] = "yellow";
	palette[32] = "cyan";
	palette[64] = "darkRed";
	palette[128] = "darkCyan";
	palette[256] = "gray";
	palette[512] = "darkGreen";
	palette[1024] = "red";
	palette[2048] = "blue";
	palette[4096] = "darkBlue";
	palette[8192] = "black";
}

void Fun2048::updateUI()
{
	auto state = board->boardState();
	int size = state.size();
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			gridbuttons[i * size + j]->setText(state[i][j] != 0 ? QString::number(state[i][j]) : "");
			gridbuttons[i * size + j]->setStyleSheet(QString("background-color: %1; font-size:40px").arg(palette[state[i][j]]));
		}
	}
}

void Fun2048::on_pushButton_clicked()
{
	ui->widget->show();
	board->resetBoard();
	updateUI();
	ui->label->setText("continue");
	ui->label->setStyleSheet("background-color: pink;font-size:30px");
}

void Fun2048::keyPressEvent(QKeyEvent *event)
{
	int key = event->key();
	switch (key)
	{
	case Qt::Key_Left:
	case Qt::Key_A:
		board->slide(Left);
		break;
	case Qt::Key_Right:
	case Qt::Key_D:
		board->slide(Right);
		break;
	case Qt::Key_Up:
	case Qt::Key_W:
		board->slide(Up);
		break;
	case Qt::Key_Down:
	case Qt::Key_S:
		board->slide(Down);
		break;
	default:
		break;
	}
	updateUI();

	GameState result = board->checkState();
	if (result == GameState::Lose)
	{
		ui->label->setText("failure");
		ui->label->setStyleSheet("background-color: red;font-size:30px");
		ui->pushButton->setText("restart");
	}
	else
	{
		ui->label->setText("continue");
		ui->label->setStyleSheet("background-color: pink;font-size:30px");
	}
}

bool Fun2048::eventFilter(QObject *obj, QEvent *ev)
{
	if(ev->type() == QEvent::KeyPress)
	{
		keyPressEvent(dynamic_cast<QKeyEvent*>(ev));
		return true;
	}
	return QWidget::eventFilter(obj, ev);
}