#ifndef FUN2048_H
#define FUN2048_H

#include <QWidget>
#include <QMap>

namespace Ui{
class Fun2048;
}
class QPushButton;
class Board;

class Fun2048 : public QWidget
{
	Q_OBJECT

public:
	explicit Fun2048(QWidget *parent = 0);
	~Fun2048();

	void updateUI();

protected:
	void keyPressEvent(QKeyEvent *event);
	bool eventFilter(QObject *obj, QEvent *ev);

private slots:
	void on_pushButton_clicked();

private:
	void createPalette();

private:
	Ui::Fun2048 *ui;
	Board *board;
	QList<QPushButton *> gridbuttons;
	QMap<int, QString> palette;
};

#endif // FUN2048_H
