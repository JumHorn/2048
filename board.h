#ifndef BOARD_H
#define BOARD_H

#include <vector>
using namespace std;

enum Operation
{
	Left,
	Right,
	Up,
	Down
};

enum GameState
{
	Win = 0,
	Lose,
	Continue
};

class Board
{
public:
	Board(int size = 4);
	~Board();

	void resetBoard(); //重置
	GameState checkState();
	vector<vector<int>> boardState();
	int getLastScore();
	void slide(Operation op);

private:
	void addOneNumber();

private:
	vector<vector<int>> data; // 0 表示空 其他表示数字
};

#endif // BOARD_H