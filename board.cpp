#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "board.h"

Board::Board(int size) : data(size, vector<int>(size))
{
	srand((unsigned int)time(NULL));
}

Board::~Board()
{
}

void Board::resetBoard()
{
	for (auto &v : data)
	{
		for (auto &n : v)
			n = 0;
	}
	addOneNumber();
}

GameState Board::checkState()
{
	int size = data.size();
	for (int i = 0; i < size; i++) //empty grid exist
	{
		for (int j = 0; j < size; j++)
		{
			if (data[i][j] == 0)
				return Continue;
		}
	}

	for (int i = 0; i < size; i++) // horizatal can be cancled out
	{
		for (int j = 1; j < size; j++)
		{
			if (data[i][j] == data[i][j - 1])
				return Continue;
		}
	}

	for (int j = 0; j < size; j++) // vertical can be cancle
	{
		for (int i = 1; i < size; i++)
		{
			if (data[i - 1][j] == data[i][j])
				return Continue;
		}
	}
	return Lose;
}

vector<vector<int>> Board::boardState()
{
	return data;
}

int Board::getLastScore()
{
	int res = 0;
	for (auto &v : data)
		res = max(res, *max_element(v.begin(), v.end()));
	return res;
}

void Board::slide(Operation op)
{
	int size = data.size();
	bool flag = false; // the same as the old board
	if (op == Left)
	{
		for (int k = 0; k < size; k++)
		{
			for (int i = 0, j = 1; j < size; j++)
			{
				if (data[k][j] == 0)
					continue;
				int old = data[k][j];
				data[k][j] = 0;
				if (data[k][i] == old)
				{
					data[k][i] *= 2;
					++i;
					flag = true;
				}
				else
				{
					if (data[k][i] == 0)
					{
						data[k][i] = old;
						flag = true;
					}
					else
					{
						data[k][++i] = old;
						if (i != j)
							flag = true;
					}
				}
			}
		}
	}
	else if (op == Right)
	{
		for (int k = 0; k < size; k++)
		{
			for (int i = size - 1, j = size - 2; j >= 0; j--)
			{
				if (data[k][j] == 0)
					continue;
				int old = data[k][j];
				data[k][j] = 0;
				if (data[k][i] == old)
				{
					data[k][i] *= 2;
					--i;
					flag = true;
				}
				else
				{
					if (data[k][i] == 0)
					{
						data[k][i] = old;
						flag = true;
					}
					else
					{
						data[k][--i] = old;
						if (i != j)
							flag = true;
					}
				}
			}
		}
	}
	else if (op == Up)
	{
		for (int k = 0; k < size; k++)
		{
			for (int i = 0, j = 1; j < size; j++)
			{
				if (data[j][k] == 0)
					continue;
				int old = data[j][k];
				data[j][k] = 0;
				if (data[i][k] == old)
				{
					data[i][k] *= 2;
					++i;
					flag = true;
				}
				else
				{
					if (data[i][k] == 0)
					{
						data[i][k] = old;
						flag = true;
					}
					else
					{
						data[++i][k] = old;
						if (i != j)
							flag = true;
					}
				}
			}
		}
	}
	else
	{
		for (int k = 0; k < size; k++)
		{
			for (int i = size - 1, j = size - 2; j >= 0; j--)
			{
				if (data[j][k] == 0)
					continue;
				int old = data[j][k];
				data[j][k] = 0;
				if (data[i][k] == old)
				{
					data[i][k] *= 2;
					--i;
					flag = true;
				}
				else
				{
					if (data[i][k] == 0)
					{
						data[i][k] = old;
						flag = true;
					}
					else
					{
						data[--i][k] = old;
						if (i != j)
							flag = true;
					}
				}
			}
		}
	}

	if (flag)
		addOneNumber();
}

void Board::addOneNumber()
{
	vector<int> index;
	int size = data.size();
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (data[i][j] == 0)
				index.push_back(i * size + j);
		}
	}

	if (!index.empty())
	{
		int empty = index[rand() % index.size()];
		data[empty / size][empty % size] = (rand() % 2 + 1) * 2;
	}
}